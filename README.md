# Flectra Community / l10n-france

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_fr_chorus_account](l10n_fr_chorus_account/) | 2.0.1.0.0| Generate Chorus-compliant e-invoices and transmit them via the Chorus API
[l10n_fr_mis_reports](l10n_fr_mis_reports/) | 2.0.1.0.2| MIS Report templates for the French P&L and Balance Sheets
[account_statement_import_fr_cfonb](account_statement_import_fr_cfonb/) | 2.0.1.1.0| Import CFONB bank statements files in Odoo
[l10n_fr_hr_check_ssnid](l10n_fr_hr_check_ssnid/) | 2.0.1.0.1| Check validity of Social Security Numbers in French companies
[l10n_fr_department_oversea](l10n_fr_department_oversea/) | 2.0.1.0.1| Populate Database with overseas French Departments (Départements d'outre-mer)
[l10n_fr_pos_cert_allow_change_qty](l10n_fr_pos_cert_allow_change_qty/) | 2.0.1.0.1| Remove restriction on change quantity
[l10n_fr_intrastat_product](l10n_fr_intrastat_product/) | 2.0.2.0.0| DEB (Déclaration d'Échange de Biens) for France
[l10n_fr_department](l10n_fr_department/) | 2.0.1.0.1| Populate Database with French Departments (Départements)
[l10n_fr_cog](l10n_fr_cog/) | 2.0.1.0.0| Add Code Officiel Géographique (COG) on countries
[l10n_fr_chorus_sale](l10n_fr_chorus_sale/) | 2.0.1.0.0| Set public market on sale orders
[l10n_fr_chorus_facturx](l10n_fr_chorus_facturx/) | 2.0.1.0.0| Generate Chorus-compliant Factur-X invoices
[account_balance_ebp_csv_export](account_balance_ebp_csv_export/) | 2.0.1.0.3| Account Balance EBP CSV export
[l10n_fr_fec_oca](l10n_fr_fec_oca/) | 2.0.1.0.2| Fichier d'Échange Informatisé (FEC) for France
[l10n_fr_das2](l10n_fr_das2/) | 2.0.1.1.0| DAS2 (France)
[l10n_fr_business_document_import](l10n_fr_business_document_import/) | 2.0.1.0.2| Adapt the module base_business_document_import for France
[account_banking_fr_lcr](account_banking_fr_lcr/) | 2.0.1.0.2| Create French LCR CFONB files
[l10n_fr_hr_rup](l10n_fr_hr_rup/) | 2.0.1.0.2|  French fields and report for Registre Unique du Personnel 
[l10n_fr_siret](l10n_fr_siret/) | 2.0.1.3.1| French company identity numbers SIRET/SIREN/NIC
[l10n_fr_account_tax_unece](l10n_fr_account_tax_unece/) | 2.0.1.0.2| Auto-configure UNECE params on French taxes
[l10n_fr_account_invoice_facturx](l10n_fr_account_invoice_facturx/) | 2.0.1.0.0| France-specific module to generate Factur-X invoices
[l10n_fr_account_invoice_import_facturx](l10n_fr_account_invoice_import_facturx/) | 2.0.1.0.2| France-specific module to import Factur-X invoices
[l10n_fr_siret_lookup](l10n_fr_siret_lookup/) | 2.0.1.1.0| Lookup partner via an API on the SIRENE directory
[l10n_fr_intrastat_service](l10n_fr_intrastat_service/) | 2.0.1.1.0| Module for Intrastat service reporting (DES) for France
[l10n_fr_state](l10n_fr_state/) | 2.0.1.0.1| Populate Database with French States (Régions)


